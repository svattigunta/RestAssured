package JobAPI;

import RestAssuredUtlity.Xutility;
import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;
import static org.testng.Assert.assertEquals;

import java.io.File;
import java.io.IOException;

import org.json.simple.JSONObject;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import io.restassured.module.jsv.JsonSchemaValidator;
import io.restassured.response.Response;

public class PostJobRequest {

	@SuppressWarnings("unchecked")
	@Test(dataProvider="JOBSAPI")
	public void postjob(String JobId,String JobTitle,String JobCompanyName,String JobLocation,String JobType,String JobPostedtime,String JobDescription)
	{		
		  baseURI="https://jobs123.herokuapp.com"; 
		  JSONObject requestparam=new JSONObject();
		  requestparam.put("Job Id",JobId); 
		  requestparam.put("Job Title",JobTitle);
		  requestparam.put("Job Company Name",JobCompanyName);
		  requestparam.put("Job Location",JobLocation);
		  requestparam.put("Job Type",JobType);
		  requestparam.put("Job Posted time",JobPostedtime);
		  requestparam.put("Job Description",JobDescription);
		  
		  Response postrequest=given().auth().basic("admin", "password")
		  .contentType("application/json").body(requestparam.toJSONString())
		  .when().post("/Jobs").then().log().all().extract().response();
		  
		  String responsebody=postrequest.getBody().asString();
		  
		  int statuscode=postrequest.statusCode();
		  if (statuscode==200)
		  {
		  assertEquals(postrequest.contentType(),"application/json");
		  assertEquals(postrequest.getStatusLine(),"HTTP/1.1 200 OK");
		  assertEquals(responsebody.contains("Florida"),true);
		  System.out.println("Response:"+responsebody);
		  
		 } else if (statuscode==409)
		 {
			 System.out.println("Jobs Already Exists");
			 assertEquals(postrequest.contentType(),"application/json");
			 assertEquals(postrequest.getStatusLine(),"HTTP/1.1 409 CONFLICT");
			 System.out.println("Response:"+responsebody);
		 }
		  
		  
			given().when().then().body(JsonSchemaValidator.
			matchesJsonSchema(new File("src/test/resources/ValidateSchema/GetJobs.Json")));
			System.out.println("Schema Validation is Successfull");
	}

	@DataProvider(name = "JOBSAPI")
	public String[][] getjobs() throws IOException {
		System.out.println("Creating Job Using Excel");
		String path = "C:/Sunandha/Eclipse/RestAssuredAPITesting/src/test/resources/XlsData/JOBSAPI.xlsx";
		Xutility xlutil = new Xutility(path);
		int totalrows = xlutil.getRowCount("PostJob");
		int totalcols = xlutil.getCellCount("PostJob", 1);
		//System.out.println(totalrows + "," + totalcols);
		String[][] JobsPost = new String[totalrows][totalcols];
		for (int i = 1; i <= totalrows; ++i) {
			for (int j = 0; j < totalcols; ++j) {
				JobsPost[i - 1][j] = xlutil.getCellData("PostJob", i, j);

			}
		}
		return JobsPost;
	}
}
